Attribute VB_Name = "mdlPoker"
Option Explicit
'==============================================================================
'最初に配られた５枚のカードでポーカーの役ができる確率を実験で求める
'このモジュールは、シャッフル、カードを配る動作をシュミレーションし、
'また、そのカードでできる役を判定する。
'==============================================================================

'アプリケーションのパス
Public gAppPath As String

'BitBlt
Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal dwRop As Long) As Long
Public Const SRCCOPY = &HCC0020

Public Const CARDWIDTH As Long = 72
Public Const CARDHEIGHT As Long = 96

'ファイルのチェックコード用
Public Declare Function CalclateSum32 Lib "FileUtl.dll" (ByVal FileName As String, ByVal Start As Long, ByVal Size As Long, ByVal InitValue As Long) As Long
Public Declare Function CalclateXor32 Lib "FileUtl.dll" (ByVal FileName As String, ByVal Start As Long, ByVal Size As Long, ByVal InitValue As Long) As Long
Public Declare Function CalclateCRC32 Lib "FileUtl.dll" (ByVal FileName As String, ByVal Start As Long, ByVal Size As Long, ByVal Produce As Long, ByVal InitValue As Long, ByVal Mode As Long) As Long

'CRC計算のオプション
Public Const CRC_GENERATE As Long = &H4C11DB7       '生成多項式

Public Const CRC_CALC_WRITE As Long = 0     'CRCを計算して書き込む
Public Const CRC_CHECK_READ As Long = 1     'CRCの値を検査する
Public Const CRC_CHECK_ZERO As Long = 2     'CRCの計算をするが、終わりに0を4バイト追加してから行う

'１回のプレイで使用されるカードの枚数
Public Const NUMPLAYCARDS As Long = 5

Public Function CheckScore(ByRef lpCards() As Long) As ePokerScores
'--------------------------------------------------------------------
'５枚のカードでできる役をテーブルから検索する
'--------------------------------------------------------------------
Dim lngIndex As Long

    'インデックスを取得する。カードの並びは、その処理の中で自動的に行われる
    lngIndex = GetCardsStatusIndex(lpCards())
    
    'テーブルを読み込む
    CheckScore = glngScoreTable(lngIndex)
End Function

Public Function CheckScoreManually(ByRef lpCards() As Long) As ePokerScores
'--------------------------------------------------------------------
'５枚のカードでできる役を（テーブルを使わずに）直接計算する
'この関数は、テーブルを作成する時に使われる。また、ジョーカーを
'含まない状態でなければならない。
'--------------------------------------------------------------------
Dim N(0 To NUMPLAYCARDS - 1) As Long    'カードの数字
Dim M(0 To NUMPLAYCARDS - 1) As Long    'カードのマーク
Dim i As Long, j As Long
Dim lngMin As Long, lngIndex As Long

'役
Dim lngFlagPair As Long             'ペア役。(1=1ペア、2=1ペア、4=3カード、8=4カード、16=5カード)
Dim lngFlagStraight As Long         'ストレート(1 = ストレート、2 = ロイヤルストレート)
Dim blnFlagFlash As Boolean         'フラッシュ

Dim Results As ePokerScores

    'カードのデータを数字とマークに分解する
    For i = 0 To NUMPLAYCARDS - 1
        N(i) = lpCards(i) \ 4 + 1
        M(i) = (lpCards(i) And 3)
        If N(i) >= 14 Then
            MsgBox "カードにジョーカーが含まれています。CheckScoreManuallyWithJoker関数を通してください。"
            Stop
        End If
    Next i
    
    '数字のデータを昇順にソートする(1,2...K)
    For i = 0 To NUMPLAYCARDS - 2
        lngMin = N(i)
        lngIndex = i
        For j = i + 1 To NUMPLAYCARDS - 1
            If N(j) < lngMin Then
                lngMin = N(j)
                lngIndex = j
            End If
        Next j
        If (i <> lngIndex) Then
            N(lngIndex) = N(i)
            N(i) = lngMin
        End If
    Next i
    
    'ペア役を判定する
    '数字は昇順に並んでいるので、数字が一致するカードは連続している
    lngFlagPair = 0
    i = 0
    Do While i < NUMPLAYCARDS
        lngMin = 1
        lngIndex = N(i)
        i = i + 1
        Do While i < NUMPLAYCARDS
            If (N(i) = lngIndex) Then
                lngMin = lngMin + 1
                i = i + 1
            Else
                Exit Do
            End If
        Loop
        '数字が一致した枚数を調べる
        If lngMin = 5 Then
            '５カード
            lngFlagPair = 16
        ElseIf lngMin = 4 Then
            '４カード
            lngFlagPair = 8
        ElseIf lngMin = 3 Then
            '３カード
            lngFlagPair = lngFlagPair Or 4
        ElseIf lngMin = 2 Then
            '１ペア
            If (lngFlagPair And 1) = 1 Then
                '既に１ペアがあるので、２ペア目
                lngFlagPair = (lngFlagPair And 28) Or 2
            Else
                lngFlagPair = lngFlagPair Or 1
            End If
        End If
    Loop
    
    'ストレートを判定する
    If ((N(1) = N(0) + 1) And (N(2) = N(1) + 1) And (N(3) = N(2) + 1) And (N(4) = N(3) + 1)) Then
        lngFlagStraight = 1     'ストレート
    End If
    If ((N(0) = 1) And (N(1) = 10) And (N(2) = 11) And (N(3) = 12) And (N(4) = 13)) Then
        lngFlagStraight = 2     'ロイヤルストレート
    End If
    
    'フラッシュを判定する
    lngMin = M(0)
    blnFlagFlash = True
    For i = 1 To NUMPLAYCARDS - 1
        If (M(i) <> lngMin) Then
            blnFlagFlash = False
            Exit For
        End If
    Next i
    
    'ペア役、ストレート、フラッシュを総合して、役を判定する
    '下のほうの役から判定していき、上位の役で上書きする
    Results = SCORE_NOSCORES
    
    '１ペア
    If lngFlagPair = 1 Then Results = SCORE_ONEPAIR
    
    '２ペア
    If lngFlagPair = 2 Then Results = SCORE_TWOPAIR
    
    '３カード
    If lngFlagPair = 4 Then Results = SCORE_THREECARD
    
    'ストレート
    If (lngFlagStraight) Then Results = SCORE_STRAIGHT
    
    'フラッシュ
    If blnFlagFlash = True Then Results = SCORE_FLASH
    
    'フルハウス
    If (lngFlagPair = 5) Then Results = SCORE_FULLHOUSE
    
    'ロイヤルストレート
    If (lngFlagStraight = 2) Then Results = SCORE_ROYALSTR
    
    '４カード
    If (lngFlagPair = 8) Then Results = SCORE_FOURCARD
    
    'ストレートフラッシュ
    If (lngFlagStraight = 1) And (blnFlagFlash = True) Then Results = SCORE_STRFLASH
    
    '５カード
    If (lngFlagPair = 16) Then Results = SCORE_FIVECARD
    
    'ロイヤルストレートフラッシュ
    If (lngFlagStraight = 2) And (blnFlagFlash = True) Then Results = SCORE_RYLSTRFLS
    
    CheckScoreManually = Results
End Function

Public Function CheckScoreManuallyWithJoker(ByRef lpCards() As Long) As ePokerScores
'--------------------------------------------------------------------
'５枚のカードでできる役を（テーブルを使わずに）直接計算する
'この関数は、テーブルを作成する時に使われる
'--------------------------------------------------------------------
Dim i As Long, j As Long
Dim lngCards() As Long
Dim lngMax As Long
Dim nPos0 As Long
Dim nPos1 As Long
Dim nResult As ePokerScores
Dim nMax As ePokerScores

    'lpCards()の中にある、ジョーカーを０から５１に置きかえて、
    '下請けのCheckScoreManuallyを呼び出す
    nPos0 = -1
    nPos1 = -1
    For i = 0 To NUMPLAYCARDS - 1
        If lpCards(i) >= 52 Then
            If nPos0 = -1 Then
                nPos0 = i
            ElseIf nPos1 = -1 Then
                nPos1 = i
            Else
                MsgBox "ジョーカーが３枚以上使われています。"
                Stop
            End If
        End If
    Next i
    
    lngCards() = lpCards()
    If nPos0 = -1 Then
        nResult = CheckScoreManually(lngCards())
    ElseIf nPos1 = -1 Then
        nMax = SCORE_NOSCORES
        For i = 0 To 51
            lngCards() = lpCards()
            lngCards(nPos0) = i
            nResult = CheckScoreManually(lngCards())
            If (nResult > nMax) Then nMax = nResult
        Next i
        nResult = nMax
    Else
        For i = 0 To 51
            For j = i To 51
                lngCards() = lpCards()
                lngCards(nPos0) = i
                lngCards(nPos1) = j
                nResult = CheckScoreManually(lngCards())
                If (nResult > nMax) Then nMax = nResult
            Next j
        Next i
        nResult = nMax
    End If
    
    CheckScoreManuallyWithJoker = nResult
End Function

Public Sub Main()

    'アプリケーションのパスを取得する
    gAppPath = App.Path
    If Right$(gAppPath, 1) = "\" Then gAppPath = Left$(gAppPath, Len(gAppPath) - 1)
    
    'テーブルをロード用意する
    ReadyIndexTable
    
    If ReadyScoreTable() = False Then
        End
    End If
    
    'メインフォームを起動する
    frmScore.Show
End Sub

VERSION 5.00
Begin VB.Form frmPoker 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ポーカーの確率計算"
   ClientHeight    =   3195
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4680
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows の既定値
End
Attribute VB_Name = "frmPoker"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
Dim nResult As ePokerScores
Dim nCards(0 To 4) As Long

    nCards(0) = 0
    nCards(1) = 1
    nCards(2) = 2
    nCards(3) = 3
    nCards(4) = 53
    nResult = CheckScoreManuallyWithJoker(nCards())
End Sub

VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProgress 
   BorderStyle     =   1  '�Œ�(����)
   Caption         =   "�i�s��"
   ClientHeight    =   2265
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7695
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2265
   ScaleWidth      =   7695
   StartUpPosition =   3  'Windows �̊���l
   Begin MSComctlLib.ProgressBar pgbStatus 
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   1080
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   873
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label lblInfo 
      BackStyle       =   0  '����
      Height          =   735
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Width           =   7455
   End
   Begin VB.Label lblPercent 
      Alignment       =   2  '��������
      BackStyle       =   0  '����
      BorderStyle     =   1  '����
      Caption         =   "0 % ����"
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   1680
      Width           =   7455
   End
End
Attribute VB_Name = "frmProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub SetPercent(ByVal lngMax As Long, ByVal lngCur As Long)

    If lngMax < lngCur Then
        lngMax = lngCur
    End If
    If lngMax <= 0 Then
        lngMax = 1
    End If
    
    With pgbStatus
        .Max = lngMax
        .Min = 0
        .Value = lngCur
    End With
    
    lblPercent.Caption = Format$(lngCur, "#,##0") & " / " & Format$(lngMax, "#,##0") & _
        "  " & Format$(CDbl(lngCur) * 100 / CDbl(lngMax), "0.000") & " % ����"
End Sub

VERSION 5.00
Begin VB.Form frmScore 
   BorderStyle     =   1  '固定(実線)
   Caption         =   "ポーカーの役　事象計算"
   ClientHeight    =   5640
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10365
   ClipControls    =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   5640
   ScaleWidth      =   10365
   StartUpPosition =   3  'Windows の既定値
   Begin VB.CommandButton cmdAuto 
      Caption         =   "自動"
      Height          =   495
      Left            =   3720
      TabIndex        =   35
      Top             =   720
      Width           =   1215
   End
   Begin VB.Frame fraCount 
      Caption         =   "事象"
      Height          =   5295
      Left            =   5760
      TabIndex        =   8
      Top             =   120
      Width           =   4455
      Begin VB.Label lblTotal 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Left            =   2760
         TabIndex        =   34
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label1 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "全事象："
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   11
         Left            =   2760
         TabIndex        =   32
         Top             =   4920
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   11
         Left            =   120
         TabIndex        =   31
         Top             =   4920
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   10
         Left            =   2760
         TabIndex        =   30
         Top             =   4560
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   10
         Left            =   120
         TabIndex        =   29
         Top             =   4560
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   9
         Left            =   2760
         TabIndex        =   28
         Top             =   4200
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   9
         Left            =   120
         TabIndex        =   27
         Top             =   4200
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   8
         Left            =   2760
         TabIndex        =   26
         Top             =   3840
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   8
         Left            =   120
         TabIndex        =   25
         Top             =   3840
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   7
         Left            =   2760
         TabIndex        =   24
         Top             =   3480
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   7
         Left            =   120
         TabIndex        =   23
         Top             =   3480
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   6
         Left            =   2760
         TabIndex        =   22
         Top             =   3120
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   6
         Left            =   120
         TabIndex        =   21
         Top             =   3120
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   5
         Left            =   2760
         TabIndex        =   20
         Top             =   2760
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   5
         Left            =   120
         TabIndex        =   19
         Top             =   2760
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   4
         Left            =   2760
         TabIndex        =   18
         Top             =   2400
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   4
         Left            =   120
         TabIndex        =   17
         Top             =   2400
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   3
         Left            =   2760
         TabIndex        =   16
         Top             =   2040
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   3
         Left            =   120
         TabIndex        =   15
         Top             =   2040
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   2
         Left            =   2760
         TabIndex        =   14
         Top             =   1680
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   2
         Left            =   120
         TabIndex        =   13
         Top             =   1680
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   1
         Left            =   2760
         TabIndex        =   12
         Top             =   1320
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   1
         Left            =   120
         TabIndex        =   11
         Top             =   1320
         Width           =   2535
      End
      Begin VB.Label lblCount 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "0"
         Height          =   255
         Index           =   0
         Left            =   2760
         TabIndex        =   10
         Top             =   960
         Width           =   1575
      End
      Begin VB.Label lblName 
         Alignment       =   1  '右揃え
         BorderStyle     =   1  '実線
         Caption         =   "ロイヤルストレートフラッシュ："
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   9
         Top             =   960
         Width           =   2535
      End
   End
   Begin VB.PictureBox picCard 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      ClipControls    =   0   'False
      Height          =   5760
      Left            =   6360
      ScaleHeight     =   384
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   1008
      TabIndex        =   7
      Top             =   6960
      Width           =   15120
   End
   Begin VB.PictureBox picField 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'なし
      ClipControls    =   0   'False
      Height          =   1440
      Left            =   120
      ScaleHeight     =   96
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   360
      TabIndex        =   5
      Top             =   1680
      Width           =   5400
   End
   Begin VB.CommandButton cmdStep 
      Caption         =   "&Step"
      Height          =   495
      Left            =   1800
      TabIndex        =   4
      Top             =   720
      Width           =   1215
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "&Reset"
      Height          =   495
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   1215
   End
   Begin VB.OptionButton optJoker 
      BackColor       =   &H00FFFFFF&
      Caption         =   "ジョーカー２枚"
      Height          =   375
      Index           =   2
      Left            =   3720
      TabIndex        =   2
      Top             =   120
      Width           =   1575
   End
   Begin VB.OptionButton optJoker 
      BackColor       =   &H00FFFFFF&
      Caption         =   "ジョーカー１枚"
      Height          =   375
      Index           =   1
      Left            =   1920
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
   Begin VB.OptionButton optJoker 
      BackColor       =   &H00FFFFFF&
      Caption         =   "ジョーカー無し"
      Height          =   375
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1575
   End
   Begin VB.Label lblScore 
      Alignment       =   2  '中央揃え
      BorderStyle     =   1  '実線
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   12
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   120
      TabIndex        =   6
      Top             =   3240
      Width           =   5415
   End
End
Attribute VB_Name = "frmScore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mNumJokers As Long
Private mCurCard(0 To NUMPLAYCARDS - 1) As Long

Private mCurrentTotal As Long
Private mScoreCount(0 To 11) As Long


Private Sub DrawCards(ByRef lpCards() As Long)
Dim i As Long
Dim X As Long, Y As Long
Dim SX As Long, SY As Long
Dim DX As Long, DY As Long
Dim rc As Long

    For i = 0 To NUMPLAYCARDS - 1
        DX = i * CARDWIDTH
        DY = 0
        
        X = lpCards(i) \ 4
        Y = (lpCards(i) And 3)
        SX = X * CARDWIDTH
        SY = Y * CARDHEIGHT
        
        rc = BitBlt(picField.hDC, DX, DY, CARDWIDTH, CARDHEIGHT, picCard.hDC, SX, SY, SRCCOPY)
    Next i
    
    picField.Refresh
End Sub

Private Sub DrawScores()
Dim i As Long
Dim lngCheck As Long

    For i = 0 To 11
        lblCount(i).Caption = mScoreCount(i)
        lngCheck = lngCheck + mScoreCount(i)
    Next i
    
    If lngCheck <> mCurrentTotal Then
        MsgBox "合計が一致しません。", , "全事象"
        Stop
    End If
    
    lblTotal.Caption = mCurrentTotal
End Sub

Private Function LoadScoreCheckData(ByVal FileName As String) As Boolean
Dim FN As Long
Dim lngJoker As Long

    If Dir$(FileName) = "" Then
        LoadScoreCheckData = False
        Exit Function
    End If
    
    FN = FreeFile
    Open FileName For Binary As #FN
        Get #FN, 65, lngJoker
        optJoker(lngJoker).Value = True
        
        Get #FN, 69, mCurrentTotal
        Get #FN, 81, mCurCard()
        Get #FN, 129, mScoreCount()
    Close #FN
    
    For lngJoker = 0 To 2
        optJoker(lngJoker).Enabled = False
    Next lngJoker
    
    LoadScoreCheckData = True
End Function

Private Sub SaveScoreCheckData(ByVal FileName As String)
Dim FN As Long
Dim lngHeader() As Long

    FN = FreeFile
    Open FileName For Binary As #FN
        ReDim lngHeader(0 To 63)
        Put #FN, 1, lngHeader()
        
        Put #FN, 65, mNumJokers
        Put #FN, 69, mCurrentTotal
        
        Put #FN, 81, mCurCard()
        
        Put #FN, 129, mScoreCount()
    Close #FN
End Sub

Private Sub cmdAuto_Click()

    Do While True
        DoEvents
        cmdStep_Click
        If (mCurCard(0) >= 48 + mNumJokers) Then
            Exit Do
        End If
    Loop
End Sub

Private Sub cmdReset_Click()
Dim i As Long

    For i = 0 To 2
        optJoker(i).Enabled = True
    Next i
    
    For i = 0 To NUMPLAYCARDS - 1
        mCurCard(i) = i
    Next i
    
    mCurrentTotal = 0
    Erase mScoreCount()
    
    DrawCards mCurCard
    DrawScores
End Sub

Private Sub cmdStep_Click()
Dim i As Long
Dim nResult As ePokerScores

    If optJoker(i).Enabled = False Then
        mCurCard(4) = mCurCard(4) + 1
        If mCurCard(4) >= 52 + mNumJokers Then
            mCurCard(3) = mCurCard(3) + 1
            If mCurCard(3) >= 51 + mNumJokers Then
                mCurCard(2) = mCurCard(2) + 1
                If mCurCard(2) >= 50 + mNumJokers Then
                    mCurCard(1) = mCurCard(1) + 1
                    If mCurCard(1) >= 49 + mNumJokers Then
                        mCurCard(0) = mCurCard(0) + 1
                        If mCurCard(0) >= 48 + mNumJokers Then
                            MsgBox "全パターンの調査が完了しました。"
                            Exit Sub
                        End If
                        mCurCard(1) = mCurCard(0) + 1
                    End If
                    mCurCard(2) = mCurCard(1) + 1
                End If
                mCurCard(3) = mCurCard(2) + 1
            End If
            mCurCard(4) = mCurCard(3) + 1
        End If
    Else
        For i = 0 To 2
            optJoker(i).Enabled = False
        Next i
    End If
    
    DrawCards mCurCard
    
    '役の計算
    nResult = CheckScore(mCurCard())
    If nResult < 0 Or nResult > 11 Then
        MsgBox "役の計算に失敗しました。テーブルが壊れています。"
        Stop
    End If
    
    lblScore.Caption = gstrScoreName(nResult)
    mScoreCount(nResult) = mScoreCount(nResult) + 1
    mCurrentTotal = mCurrentTotal + 1
    
    If nResult = SCORE_NOSCORES Then
        lblScore.ForeColor = &H0&
    Else
        lblScore.ForeColor = &HFF&
    End If
    
    DrawScores
End Sub

Private Sub Form_Load()
Dim i As Long

    With picCard
        .Width = CARDWIDTH * 14 * Screen.TwipsPerPixelX
        .Height = CARDHEIGHT * 4 * Screen.TwipsPerPixelY
        .Picture = LoadPicture(gAppPath & "\Cards.bmp")
    End With
    
    With picField
        .Width = CARDWIDTH * NUMPLAYCARDS * Screen.TwipsPerPixelX
        .Height = CARDHEIGHT * Screen.TwipsPerPixelY
    End With
    
    If LoadScoreCheckData(gAppPath & "\Score.dat") = False Then
        cmdReset_Click
    End If
    
    For i = 0 To 11
        lblName(i).Caption = gstrScoreName(i)
    Next i
    
    DrawCards mCurCard()
    DrawScores
End Sub

Private Sub Form_Unload(Cancel As Integer)
    SaveScoreCheckData gAppPath & "\Score.dat"
End Sub

Private Sub optJoker_Click(Index As Integer)
    mNumJokers = Index
    optJoker(mNumJokers).Value = True
End Sub

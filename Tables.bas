Attribute VB_Name = "mdlTables"
Option Explicit
'==============================================================================
'役の計算に必要なテーブル
'==============================================================================

'役テーブル
Public glngScoreTable() As Long

'カードの組み合わせ
Public Const NUMCARDCOMBINATIONS As Long = 3162510

'コンビネーションテーブル
Public glngS0Table() As Long
Public glngS1Table() As Long
Public glngS2Table() As Long
Public glngS3Table() As Long

'役一覧
Public Enum ePokerScores
    SCORE_NOSCORES = 0          '役無し
    SCORE_ONEPAIR = 1           '１ペア
    SCORE_TWOPAIR = 2           '２ペア
    SCORE_THREECARD = 3         '３カード
    SCORE_STRAIGHT = 4          'ストレート
    SCORE_FLASH = 5             'フラッシュ
    SCORE_FULLHOUSE = 6         'フルハウス
    SCORE_ROYALSTR = 7          'ロイヤルストレート
    SCORE_FOURCARD = 8          '４カード
    SCORE_STRFLASH = 9          'ストレートフラッシュ
    SCORE_FIVECARD = 10         '５カード
    SCORE_RYLSTRFLS = 11        'ロイヤルストレートフラッシュ
End Enum
Public gstrScoreName() As String

'プログレスウィンドウ
Public gobjProgress As frmProgress

Public Function Combination(ByVal N As Long, ByVal R As Long) As Long
Dim i As Long
Dim Y As Long

    Y = 1
    For i = N - R + 1 To N
        Y = Y * i
    Next i
    For i = 1 To R
        If (Y Mod i) <> 0 Then Stop
        Y = Y \ i
    Next i
    
    Combination = Y
End Function

Public Function GetCardsStatusIndex(ByRef lpCards() As Long) As Long
'--------------------------------------------------------------------
'５枚のカードの組み合わせから、テーブルのインデックスを計算する
'--------------------------------------------------------------------
Dim i As Long, j As Long
Dim lngMin As Long, lngIndex As Long
Dim lngSortCards() As Long
Dim S0 As Long, S1 As Long, S2 As Long
Dim S3 As Long, S4 As Long

    'カードのデータをソートする
    lngSortCards() = lpCards()
    
    For i = 0 To NUMPLAYCARDS - 2
        lngMin = lngSortCards(i)
        lngIndex = i
        For j = i + 1 To NUMPLAYCARDS - 1
            If lngSortCards(j) = lngMin Then
                MsgBox "同一カードを２枚使用しています。"
                Stop
            End If
            If lngSortCards(j) < lngMin Then
                lngMin = lngSortCards(j)
                lngIndex = j
            End If
        Next j
        lngSortCards(lngIndex) = lngSortCards(i)
        lngSortCards(i) = lngMin
    Next i
        
                
    'インデックスを計算する
    S0 = glngS0Table(lngSortCards(0))
    S1 = glngS1Table(lngSortCards(0), lngSortCards(1))
    S2 = glngS2Table(lngSortCards(1), lngSortCards(2))
    S3 = glngS3Table(lngSortCards(2), lngSortCards(3))
    S4 = (lngSortCards(4) - lngSortCards(3) - 1)
    
    If (S0 < 0) Or (S1 < 0) Or (S2 < 0) Or (S3 < 0) Or (S4 < 0) Then
        MsgBox "テーブルのインデックスを計算中に予期せぬエラーが発生しました。"
        Stop
    End If
    
    GetCardsStatusIndex = S0 + S1 + S2 + S3 + S4
End Function

Public Function LoadScoreTable(ByVal FileName As String) As Boolean
'--------------------------------------------------------------------
'役テーブルをファイルから読み込む
'--------------------------------------------------------------------
Dim FN As Long
Dim i As Long
Dim lngHeader() As Long
Dim bytBuffer() As Byte
Dim lngFileLen As Long
Dim lngCRC As Long

    gobjProgress.Show
    gobjProgress.lblInfo.Caption = "テーブルをファイルから読み込んでいます。"
    gobjProgress.Refresh
    DoEvents
    
    If Dir$(FileName) = "" Then
        'ファイルが見つからない
        LoadScoreTable = False
        Exit Function
    End If
    
    'データが壊れていないか検査する
    lngCRC = CalclateCRC32(FileName, 0, 64, CRC_GENERATE, 0, CRC_CHECK_READ)
    If lngCRC Then
        MsgBox "ヘッダが壊れています。" & vbCrLf & FileName, vbOKOnly, "CRCエラー"
        LoadScoreTable = False
        Exit Function
    End If
    
    lngFileLen = FileLen(FileName)
    lngCRC = CalclateCRC32(FileName, 0, lngFileLen, CRC_GENERATE, 0, CRC_CHECK_READ)
    If lngCRC Then
        MsgBox "ファイルが壊れています。" & vbCrLf & FileName, vbOKOnly, "CRCエラー"
        LoadScoreTable = False
        Exit Function
    End If
    
    FN = FreeFile
    Open FileName For Binary As #FN
        'ヘッダを検査する
        ReDim lngHeader(0 To 15)
        Get #FN, 1, lngHeader()
        
        If lngHeader(0) <> &H1A545350 Then
            MsgBox "ファイルが壊れています。" & vbCrLf & FileName, vbOKOnly, "ヘッダエラー"
            Close #FN
            LoadScoreTable = False
            Exit Function
        End If
        
        If lngHeader(4) <> NUMCARDCOMBINATIONS Then
            MsgBox "カードの組み合わせの全パターンが記録されていません。", vbOKOnly, "ヘッダエラー"
            Close #FN
            LoadScoreTable = False
            Exit Function
        End If
        
        Seek #FN, 65
        ReDim glngScoreTable(0 To NUMCARDCOMBINATIONS - 1)
        ReDim bytBuffer(0 To NUMCARDCOMBINATIONS - 1)
        
        gobjProgress.SetPercent NUMCARDCOMBINATIONS + 1, 1
        gobjProgress.Refresh
        DoEvents
        
        Get #FN, , bytBuffer()
        
        For i = 0 To NUMCARDCOMBINATIONS - 1
            glngScoreTable(i) = bytBuffer(i)
            If ((i And 262143) = 0) Then
                gobjProgress.SetPercent NUMCARDCOMBINATIONS + 1, i + 1
                DoEvents
            End If
        Next i
    Close #FN
    
    LoadScoreTable = True
End Function

Public Function MakeScoreTable() As Boolean
Dim blnSuccess As Boolean
Dim MsgAns As VbMsgBoxResult

Dim lngCards(0 To 4) As Long
Dim C0 As Long, C1 As Long, C2 As Long, C3 As Long, C4 As Long
Dim lngIndex As Long, lngScore As ePokerScores

Dim FN As Long
Dim blnFlagDummy(0 To NUMCARDCOMBINATIONS - 1) As Boolean
Dim lngScoreCount(0 To 11) As Long, lngTotal As Long

    '配列の要素を確保する
    ReDim glngScoreTable(0 To NUMCARDCOMBINATIONS - 1)
    
    gobjProgress.lblInfo.Caption = "テーブルを作成しています。"
    
    'テーブルを作成する
    'まず、ジョーカーを含まない場合
    For C0 = 0 To 49
        For C1 = C0 + 1 To 50
            For C2 = C1 + 1 To 51
                For C3 = C2 + 1 To 52
                    For C4 = C3 + 1 To 53
                        lngCards(0) = C0
                        lngCards(1) = C1
                        lngCards(2) = C2
                        lngCards(3) = C3
                        lngCards(4) = C4
                        lngIndex = GetCardsStatusIndex(lngCards())
                        
                        If blnFlagDummy(lngIndex) = True Then
                            MsgBox "テーブルの同一インデックスが多重に使用されています。"
                            Stop
                            lngIndex = GetCardsStatusIndex(lngCards())
                        End If
                        lngScore = CheckScoreManuallyWithJoker(lngCards())
                        glngScoreTable(lngIndex) = lngScore
                        
                        lngScoreCount(lngScore) = lngScoreCount(lngScore) + 1
                        lngTotal = lngTotal + 1
                        blnFlagDummy(lngIndex) = True
                    Next C4
                Next C3
                gobjProgress.SetPercent NUMCARDCOMBINATIONS, lngTotal
                DoEvents
            Next C2
        Next C1
    Next C0
    
    '作成したテーブルをファイルに保存する
    blnSuccess = False
    Do Until blnSuccess
        blnSuccess = SaveScoreTable(gAppPath & "\Scores.tbl")
        If blnSuccess = False Then
            MsgAns = MsgBox("作成したテーブルを保存できませんでした。再試行しますか？", vbYesNo, "再試行")
            If MsgAns = vbNo Then Exit Do
        End If
    Loop
    
    FN = FreeFile
    Open gAppPath & "\Scores.txt" For Output As #FN
        Print #FN, "役　　　　　　　　　　　　　：パターン総数"
        Print #FN, "　　　　　　　　　　　役無し：" & lngScoreCount(0)
        Print #FN, "　　　　　　　　　　　１ペア：" & lngScoreCount(1)
        Print #FN, "　　　　　　　　　　　２ペア：" & lngScoreCount(2)
        Print #FN, "　　　　　　　　　　３カード：" & lngScoreCount(3)
        Print #FN, "　　　　　　　　　ストレート：" & lngScoreCount(4)
        Print #FN, "　　　　　　　　　フラッシュ：" & lngScoreCount(5)
        Print #FN, "　　　　　　　　　フルハウス：" & lngScoreCount(6)
        Print #FN, "　　　　　ロイヤルストレート：" & lngScoreCount(7)
        Print #FN, "　　　　　　　　　　４カード：" & lngScoreCount(8)
        Print #FN, "　　　　ストレートフラッシュ：" & lngScoreCount(9)
        Print #FN, "　　　　　　　　　　５カード：" & lngScoreCount(10)
        Print #FN, "ロイヤルストレートフラッシュ：" & lngScoreCount(11)
        Print #FN, "　　　　　　　　　　　　合計：" & lngTotal
    Close #FN
    
    MakeScoreTable = blnSuccess
End Function

Public Sub ReadyIndexTable()
'--------------------------------------------------------------------
'インデックステーブルを用意する
'--------------------------------------------------------------------
Dim i As Long
Dim j As Long
Dim C As Long

    '１枚目のカードに対するインデックス
    ReDim glngS0Table(0 To 49)
    C = 0
    For i = 0 To 49
        glngS0Table(i) = C
        C = C + Combination(53 - i, 4)
    Next i
    
    '２枚目のカードに対するインデックス
    ReDim glngS1Table(0 To 49, 1 To 50)
    For i = 0 To 49
        For j = 1 To i
            glngS1Table(i, j) = -1
        Next j
        C = 0
        For j = i + 1 To 50
            glngS1Table(i, j) = C
            C = C + Combination(53 - j, 3)
        Next j
    Next i

    '３枚目のカードに対するインデックス
    ReDim glngS2Table(1 To 50, 2 To 51)
    For i = 1 To 50
        For j = 2 To i
            glngS2Table(i, j) = -1
        Next j
        C = 0
        For j = i + 1 To 51
            glngS2Table(i, j) = C
            C = C + Combination(53 - j, 2)
        Next j
    Next i

    '４枚目のカードに対するインデックス
    ReDim glngS3Table(2 To 51, 3 To 52)
    For i = 2 To 51
        For j = 3 To i
            glngS3Table(i, j) = -1
        Next j
        C = 0
        For j = i + 1 To 52
            glngS3Table(i, j) = C
            C = C + (53 - j)
        Next j
    Next i
End Sub

Public Function ReadyScoreTable() As Boolean
'--------------------------------------------------------------------
'役テーブルを用意する
'--------------------------------------------------------------------

    Set gobjProgress = New frmProgress
    gobjProgress.Show
    gobjProgress.Refresh
    DoEvents
    
    '役の名前
    ReDim gstrScoreName(0 To 11)
    gstrScoreName(0) = "役無し"
    gstrScoreName(1) = "１ペア"
    gstrScoreName(2) = "２ペア"
    gstrScoreName(3) = "３カード"
    gstrScoreName(4) = "ストレート"
    gstrScoreName(5) = "フラッシュ"
    gstrScoreName(6) = "フルハウス"
    gstrScoreName(7) = "ロイヤルストレート"
    gstrScoreName(8) = "４カード"
    gstrScoreName(9) = "ストレートフラッシュ"
    gstrScoreName(10) = "５カード"
    gstrScoreName(11) = "ロイヤルストレートフラッシュ"
    
    'テーブルがファイルに保存されている場合はそれをロードする
    If LoadScoreTable(gAppPath & "\Scores.tbl") = False Then
        'ファイルがなければ、計算して作成し、ファイルに保存する
        If MakeScoreTable() = False Then
            MsgBox "テーブルのロードおよび作成ができません。"
            ReadyScoreTable = False
            Exit Function
        End If
    End If
    
    Unload gobjProgress
    Set gobjProgress = Nothing
    
    ReadyScoreTable = True
End Function

Public Function SaveScoreTable(ByVal FileName As String) As Boolean
'--------------------------------------------------------------------
'役テーブルをファイルに保存する
'--------------------------------------------------------------------
Dim i As Long
Dim FN As Long
Dim lngHeader() As Long
Dim bytBuffer() As Byte
Dim lngFileLen As Long
Dim lngCRC As Long

    '既存のファイルを削除する
    FN = FreeFile
    Open FileName For Output As #FN
    Close #FN
    
    'ヘッダを用意する
    ReDim lngHeader(0 To 15)
    lngHeader(0) = &H1A545350
    lngHeader(4) = NUMCARDCOMBINATIONS
    
    gobjProgress.lblInfo.Caption = "作成したテーブルをファイルに保存しています"
    gobjProgress.SetPercent NUMCARDCOMBINATIONS + 1, 0
    gobjProgress.Refresh
    DoEvents
    
    'ファイルをバイナリモードで開いて保存する
    FN = FreeFile
    Open FileName For Binary As #FN
        'ヘッダを書き込む
        Put #FN, 1, lngHeader()
        
        'テーブルを書き込む
        ReDim bytBuffer(0 To NUMCARDCOMBINATIONS - 1)
        For i = 0 To NUMCARDCOMBINATIONS - 1
            bytBuffer(i) = glngScoreTable(i)
            If ((i And 262143) = 0) Then
                gobjProgress.SetPercent NUMCARDCOMBINATIONS + 1, i
                DoEvents
            End If
        Next i
        
        Put #FN, 65, bytBuffer()
        
        'チェックサム用の領域を確保する
        ReDim lngHeader(0 To 15)
        Put #FN, , lngHeader()
    Close #FN
    
    'CRCを書き込む
    lngFileLen = FileLen(FileName)
    
    lngCRC = CalclateCRC32(FileName, 0, 64, CRC_GENERATE, 0, CRC_CALC_WRITE)
    lngCRC = CalclateCRC32(FileName, 0, lngFileLen, CRC_GENERATE, 0, CRC_CALC_WRITE)
    
    '書き込んだCRCを検査する
    lngCRC = CalclateCRC32(FileName, 0, 64, CRC_GENERATE, 0, CRC_CHECK_READ)
    If lngCRC Then
        SaveScoreTable = False
        Exit Function
    End If
    
    lngCRC = CalclateCRC32(FileName, 0, lngFileLen, CRC_GENERATE, 0, CRC_CHECK_READ)
    If lngCRC Then
        SaveScoreTable = False
        Exit Function
    End If
    
    '正常終了
    SaveScoreTable = True
End Function
